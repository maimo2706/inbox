import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor() {

  }
  title = 'app';

  emptyData = false
  inboxData = [
    {
      "from" : {
        "name" : "Now TV",
        "email" : "nowtv@test.com"
      },
      "subject" : "Grab another Pass, you need to be watching this...",
      "body" : "Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting the likes of Thandie Newton, James Marsden and Jeffrey Wright."
    },
    {
      "from" : {
        "name" : "Investopedia Terms",
        "email" : "investopedia@test.com"
      },
      "subject" : "What is 'Fibonanci Retracement'?",
      "body" : "Fibonacci retracement is a term used in technical analysis that refers to areas of support (price stops going lower) or resistance (price stops going higher)."
    },
    {
      "from" : {
        "name" : "ASICS Greater Manchester Marathon ",
        "email" : "events@human-race.co.uk"
      },
      "subject" : "Your chance to take on the marathon",
      "body" : "Do you feel inspired to take on one of Europe's most highly regarded and popular marathons?"
    }
  ];
  
  ngOnInit() {
    if (this.inboxData.length == 0) {
      this.emptyData = true
    }
    console.log(this.inboxData.length, this.emptyData)
  }

}

